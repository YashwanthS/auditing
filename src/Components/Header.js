import React from "react";

function Header() {
  return (
    <>
      <div className="header">
        <div
          style={{
            display: "flex",
            alignItems: "center",
            gap: 20,
            marginLeft: "25px",
          }}
        >
          <div
            className="border rounded-circle"
            style={{
              height: 36,
              width: 36,
              background: "#EBEDF9",
              display: "flex",
              alignItems: "center",
            }}
          >
            <img src="/images/arrow_back.svg" className="mx-auto" alt="back" />
          </div>

          <div style={{ fontWeight: "800", fontSize: 16 }}>Audit Findings</div>
        </div>
        <div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              gap: 20,
              marginRight: "30px",
            }}
          >
            <div>
              <img
                src="/images/notification_icon.svg"
                className="mx-auto"
                alt="back"
              />
            </div>

            <div
              className="border rounded-circle"
              style={{
                height: 36,
                width: 36,
                background: "#EBEDF9",
                display: "flex",
                alignItems: "center",
                fontWeight: 800,
                fontSize: 14,
              }}
            >
              <span className="mx-auto">GP</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;
