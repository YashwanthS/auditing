import React from "react";

function Sidebar(props) {
  return (
    <>
      <ul className="list-group-none p-2">
        <li className="list-group-item my-2">
          <div
            style={{
              height: 46,
              width: 46,
              background: "#EBEDF9",
              display: "flex",
              alignItems: "center",
              borderRadius: 10,
            }}
          >
            <span
              className="mx-auto"
              style={{ fontWeight: 800, fontSize: 18, color: "#212121" }}
            >
              MA
            </span>
          </div>
        </li>
        <li
          className="list-group-item my-4"
          onClick={() => {
            // setClick(menuItemMain.name);
          }}
        >
          <img
            src="/images/dashboard_icon.svg"
            alt="sidebar_icon"
            style={{ height: 40, width: 40 }}
          />
          <div className="sidebar_text">Dashboard</div>
        </li>
        <li className="list-group-item my-4">
          <img
            src="/images/auditing_icon.svg"
            alt="sidebar_icon"
            style={{
              height: 28,
              width: 32,
              marginLeft: 5,
            }}
          />
          <div className="sidebar_text my-2">Auditing</div>
        </li>
        <li className="list-group-item my-4">
          <img
            src="/images/messages_icon.svg"
            alt="sidebar_icon"
            style={{ height: 23, width: 23, marginLeft: 10 }}
          />
          <div className="sidebar_text my-2">Messages</div>
        </li>
        <li className="list-group-item my-4">
          <img
            src="/images/company_icon.svg"
            alt="sidebar_icon"
            style={{ height: 23, width: 23, marginLeft: 10 }}
          />
          <div className="sidebar_text my-2">Your Clients</div>
        </li>
        <li className="list-group-item" style={{ marginTop: 500 }}>
          <img
            src="/images/setting_icon.svg"
            alt="sidebar_icon"
            style={{ height: 44, width: 44, marginLeft: 5 }}
          />
          <span className="sidebar_text">Settings</span>
        </li>
        <li className="list-group-item my-5">
          <img
            src="/images/user_icon.svg"
            alt="sidebar_icon"
            style={{ height: 25, width: 26, marginLeft: 5 }}
          />
          <div className="sidebar_text">Users</div>
        </li>
      </ul>
    </>
  );
}

export default Sidebar;
