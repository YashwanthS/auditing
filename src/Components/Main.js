import React, { useState } from "react";
import Modal from "react-modal";

function Main() {
  const [modalIsOpen, setIsOpen] = useState(false);

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      // width: "70%",
      borderRadius: 10,
      padding: 30,
    },
    overlay: {
      background: "rgba(0,0,0,0.2)",
    },
  };

  function loading() {
    document.querySelectorAll(".bar").forEach(function (current) {
      let startWidth = 0;
      const endWidth = current.dataset.size;
      console.log("EndWidth>>>", endWidth);

      /* 
          setInterval() time sholud be set as trasition time / 100. 
          In our case, 2 seconds / 100 = 20 milliseconds. 
          */
      const interval = setInterval(frame, 20);
      function frame() {
        if (startWidth >= endWidth) {
          clearInterval(interval);
        } else {
          startWidth++;
          current.style.width = `${endWidth}%`;
          current.firstElementChild.innerText = `${startWidth}%`;
        }
      }
    });
  }

  setTimeout(loading, 3000);

  function ProgressBar() {
    return (
      <div>
        <div className="wrapper">
          <div className="progress-bar">
            <div className="bar" data-size="70">
              <div className="perc" style={{ color: "white" }}></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  return (
    <>
      <div className="row mx-3 mt-3">
        <div className="col-lg-8 col-sm-12">
          <div
            style={{
              paddingTop: 10,
              paddingBottom: 10,
              display: "flex",
              gap: 10,
              alignItems: "center",
            }}
          >
            <div style={{ marginRight: 20 }}>
              <div style={{ fontWeight: 800, fontSize: 16 }}>
                Star Technologies, Mumbai
              </div>
              <div style={{ fontWeight: 400, fontSize: 14 }}>
                01-Apr-2022 To 31-Mar-2023
              </div>
            </div>
            <div
              className="mx-1"
              style={{ display: "flex", alignItems: "center" }}
            >
              <span
                style={{
                  border: "1px solid black",
                  height: 70,
                }}
              ></span>

              <span className="mx-3 lg:mx-5">Run Audit</span>
              <div
                className="border rounded-circle"
                style={{
                  height: 46,
                  width: 46,
                  background: "#EBEDF9",
                  display: "flex",
                  alignItems: "center",
                }}
                onClick={() => setIsOpen(!modalIsOpen)}
              >
                <img
                  src="/images/play_button.svg"
                  alt="play"
                  className="mx-auto"
                />
              </div>

              {/* <ClickModal click={click} /> */}
            </div>
          </div>
        </div>
        <div
          className="col-lg-4  col-sm-12 justify-content-end sm:justify-content-center"
          style={{ display: "flex" }}
        >
          <div className="px-5 my-3 audit-button">Audit Manually</div>
        </div>
      </div>

      <div className="row mx-3 mt-3">
        <div className="col-lg-7 col-sm-12">
          <div className="card pr-4 pl-4">
            <div className="p-3 assets-heading">Assets / Liabilities</div>
            <div className="row m-0 my-3">
              <div className="col-6">
                <div
                  className="card my-2 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Fixed Assets</p>
                  <p className="assets-count">₹ 39,52,962</p>
                </div>
              </div>
              <div className="col-6">
                <div
                  className="card my-2 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Outstanding Loans</p>
                  <p className="assets-count">₹ 10,29,740</p>
                </div>
              </div>
              <div className="col-6">
                <div
                  className="card my-2 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Cash Balance</p>
                  <p className="assets-count">₹ 35,91,586</p>
                </div>
              </div>
              <div className="col-6">
                <div
                  className="card my-2 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Bank Balance</p>
                  <p className="assets-count">₹ 4,87,705</p>
                </div>
              </div>
            </div>
          </div>
          <div className="card pr-4 pl-4 my-2">
            <div className="row m-0">
              <div className="col-lg-6 col-sm-6">
                <div className="p-3 assets-heading">Top 5 expenses</div>
              </div>
              <div className="col-lg-6 col-sm-6 d-flex justify-content-end">
                <div className="p-3 assets-heading">
                  <div
                    style={{
                      width: 81,
                      height: 30,
                      borderRadius: 46,
                      fontWeight: 600,
                      fontSize: 12,
                      border: "1px solid #C4C9EE",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <span className="mx-auto">View All</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="d-flex">
              <div>
                <img
                  src="/images/chart_1.svg"
                  alt="chart-2"
                  className="chart-2-img"
                />
              </div>
              <div>
                <div className="row py-4">
                  <div className="col-lg-6">
                    <div className="mt-4 d-flex">
                      <div
                        className="mt-1"
                        style={{
                          border: "1px solid #F68989",
                          background: "#F68989",
                          height: 12,
                          width: 12,
                          borderRadius: "50%",
                        }}
                      ></div>
                      <div className="mx-3">
                        <div className="chart-2-text">53%</div>
                        <div className="chart-2-text">
                          Employee Benefit Expenses
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="mt-4 d-flex">
                      <div
                        className="mt-1"
                        style={{
                          border: "1px solid #C4C9EE",
                          background: "#C4C9EE",
                          height: 12,
                          width: 12,
                          borderRadius: "50%",
                        }}
                      ></div>
                      <div className="mx-3">
                        <div className="chart-2-text">4%</div>
                        <div className="chart-2-text">
                          Repairs & Maintenance
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    {" "}
                    <div className="mt-4 d-flex">
                      <div
                        className="mt-1"
                        style={{
                          border: "1px solid #FFEEB3",
                          background: "#FFEEB3",
                          height: 12,
                          width: 12,
                          borderRadius: "50%",
                        }}
                      ></div>
                      <div className="mx-3">
                        <div className="chart-2-text">25%</div>
                        <div className="chart-2-text">Rental Expenses</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="mt-4 d-flex">
                      <div
                        className="mt-1"
                        style={{
                          border: "1px solid #B3DFFF",
                          background: "#B3DFFF",
                          height: 12,
                          width: 12,
                          borderRadius: "50%",
                        }}
                      ></div>
                      <div className="mx-3">
                        <div className="chart-2-text">5%</div>
                        <div className="chart-2-text">Office Maintenance</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="mt-4 d-flex">
                      <div
                        className="mt-1"
                        style={{
                          border: "1px solid #B3FFDF",
                          background: "#B3FFDF",
                          height: 12,
                          width: 12,
                          borderRadius: "50%",
                        }}
                      ></div>
                      <div className="mx-3">
                        <div className="chart-2-text">13%</div>
                        <div className="chart-2-text">Administrative</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-5 col-sm-12">
          <div className="card pr-4 pl-4 py-3">
            <div className="p-3 assets-heading">Revenue</div>
            <div className="row m-0 py-2">
              <div className="col-6">
                <div
                  className="card my-1 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Total Turn Over</p>
                  <p className="assets-count">₹ 9,33,78,890</p>
                </div>
              </div>
              <div className="col-6">
                <div
                  className="card my-1 p-4"
                  style={{ background: "rgba(235, 237, 249, 0.6)" }}
                >
                  <p className="assets-text">Net Profit</p>
                  <p className="assets-count">₹ 19,22,664</p>
                </div>
              </div>
            </div>
            <div className="d-flex">
              <div>
                <img
                  src="/images/chart_2.svg"
                  alt="chart-2"
                  className="chart-2-img"
                />
              </div>
              <div>
                <div className="mt-5 d-flex">
                  <div
                    className="mt-1"
                    style={{
                      border: "1px solid #B3FFDF",
                      background: "#B3FFDF",
                      height: 12,
                      width: 12,
                      borderRadius: "50%",
                    }}
                  ></div>
                  <div className="mx-3">
                    <div className="chart-2-text">51%</div>
                    <div className="chart-2-text">Total Turnover</div>
                  </div>
                </div>
                <div className="mt-4 d-flex">
                  <div
                    className="mt-1"
                    style={{
                      border: "1px solid #C4C9EE",
                      background: "#C4C9EE",
                      height: 12,
                      width: 12,
                      borderRadius: "50%",
                    }}
                  ></div>
                  <div className="mx-3">
                    <div className="chart-2-text">5%</div>
                    <div className="chart-2-text">Total Indirect expenses</div>
                  </div>
                </div>
                <div className="mt-4 d-flex">
                  <div
                    style={{
                      border: "1px solid #B3DFFF",
                      background: "#B3DFFF",
                      height: 12,
                      width: 12,
                      borderRadius: "50%",
                    }}
                  ></div>
                  <div className="mx-3">
                    <div className="chart-2-text">45%</div>
                    <div className="chart-2-text">Total Purchase</div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row" style={{ marginLeft: 60 }}>
              <div className="col-lg-5 my-3 chart-2-text2">Total Purchase:</div>
              <div className="col-lg-4 my-3 chart-2-text2">
                Rs. 8,31,89,497.71
              </div>

              <div className="col-lg-5 my-3 chart-2-text2">
                Total Direct Expenses:
              </div>
              <div className="col-lg-4 my-3 chart-2-text2">
                Rs. 13,82,664.33
              </div>

              <div className="col-lg-5 my-3 chart-2-text2">
                Total Indirect Expenses:
              </div>
              <div className="col-lg-4 my-3 chart-2-text2">
                Rs. 82,66,727.96
              </div>

              <div className="col-lg-5 my-3 chart-2-text2">
                Change in Stock:
              </div>
              <div className="col-lg-4 my-3 chart-2-text2">Rs. 5,40,000</div>
            </div>
          </div>
        </div>
      </div>

      <Modal
        isOpen={modalIsOpen}
        ariaHideApp={false}
        // onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <button
          style={{
            border: "none",
            background: "none",
            marginLeft: 300,
          }}
          onClick={() => {
            setIsOpen(!modalIsOpen);
            setTimeout(loading, 3000);
          }}
        >
          <img
            src="/images/closeicon.png"
            alt="close-icon"
            style={{
              width: 15,
              height: 15,
            }}
          />
        </button>
        <div
          className="p-3"
          style={{
            background: "#FFFFFF",
            width: "18rem",
          }}
        >
          <img
            src="/images/loading.svg"
            alt="loading"
            style={{
              height: 38,
              width: 44,
              marginLeft: 120,
            }}
          />

          <div
            className="text-center"
            style={{ fontWeight: 700, fontSize: 18 }}
          >
            Auditing is in progress
          </div>
          <p
            className="text-center mt-3"
            style={{ fontWeight: 400, fontSize: 14 }}
          >
            Do not close or refresh the page. This process may take some time to
            complete
          </p>
          <div>
            <ProgressBar />
          </div>
        </div>
      </Modal>

      <div className="row mx-3 mt-3">
        <div className="col-lg-6 col-sm-12">
          <div
            className="card pr-4 pl-4 my-2 scroller"
            style={{ height: "80%" }}
          >
            <div className="row m-0">
              <div className="col-lg-5 col-sm-6">
                <div className="p-3 assets-heading">Top Debitors</div>
              </div>
              <div className="col-lg-5 col-sm-5 d-flex justify-content-end">
                <div className="p-3 assets-heading">
                  <div
                    style={{
                      width: 81,
                      height: 30,
                      borderRadius: 46,
                      fontWeight: 600,
                      fontSize: 12,
                      border: "1px solid #C4C9EE",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <span className="mx-auto">View All</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 my-3">
                <ul className="list-group-none p-0 mx-3">
                  <li className="list-group-item my-4 debitors-text">
                    Card Sales Customer:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    KP Retail:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    Orion Hospital:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    Season Travels:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    ODPC Power Limited:
                  </li>
                </ul>
              </div>
              <div className="col-lg-6 my-3 d-flex justify-content-end">
                <ul className="list-group-none">
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 11,22,923.48
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 4,17,086.0
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 1,20,345.88
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 51,000.0
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 18,498.0
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-sm-12">
          <div
            className="card pr-4 pl-4 my-2 scroller"
            style={{ height: "80%" }}
          >
            <div className="row m-0">
              <div className="col-lg-6 col-sm-6">
                <div className="p-3 assets-heading">Top Creditors</div>
              </div>
              <div className="col-lg-6 col-sm-6 d-flex justify-content-end">
                <div className="p-3 assets-heading">
                  <div
                    style={{
                      width: 81,
                      height: 30,
                      borderRadius: 46,
                      fontWeight: 600,
                      fontSize: 12,
                      border: "1px solid #C4C9EE",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <span className="mx-auto">View All</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 my-3">
                <ul className="list-group-none p-0 mx-3">
                  <li className="list-group-item my-4 debitors-text">
                    SK Mills:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    SSD Chocolates:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    RK Hotels:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    Star Bakers:
                  </li>
                  <li className="list-group-item my-4 debitors-text">
                    Season Travels:
                  </li>
                </ul>
              </div>
              <div className="col-lg-6 my-3 d-flex justify-content-end">
                <ul className="list-group-none">
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 11,17,1443.21
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 6,73,521.0
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 5,32,128.0
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 4,77,497.0
                  </li>
                  <li className="list-group-item my-4 debitors-text-rupee">
                    Rs. 3,81,871.0
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Main;

// function ClickModal(props) {
//   const click = props.click;
//   if (!click) {
//     return null;
//   }
//   return (
//     <>

//       <div
//         style={{
//           top: "50%",
//           left: "50%",
//           right: "auto",
//           bottom: "auto",
//           marginRight: "-50%",
//           transform: "translate(-50%, -50%)",
//           width: "70%",
//           position: "relative",
//           borderRadius: 36,
//           zIndex: 10,
//           padding: 30,
//           overlay: {
//             background: "rgba(0,0,0,0.2)",
//           },
//         }}
//       >
//         <div
//           className="card p-3"
//           style={{
//             background: "#FFFFFF",
//             width: "18rem",
//           }}
//         >
//           <img
//             src="/images/loading.svg"
//             alt="loading"
//             className="mx-auto"
//             style={{ height: 38, width: 44 }}
//           />
//           <div className="mx-auto">
//             <div
//               className="text-center"
//               style={{ fontWeight: 700, fontSize: 18 }}
//             >
//               Auditing is in progress
//             </div>
//             <p
//               className="text-center mt-3"
//               style={{ fontWeight: 400, fontSize: 14 }}
//             >
//               Do not close or refresh the page. This process may take some time
//               to complete
//             </p>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// }
