import "bootstrap/dist/css/bootstrap.css";
import "./App.css";
import Header from "./Components/Header";
import Main from "./Components/Main";
import Sidebar from "./Components/Sidebar";

function App() {
  return (
    <div className="App">
      <div className="sidebar">
        <Sidebar />
      </div>
      <div className="main">
        <div className="header">
          <Header />
        </div>
        <div>
          <Main />
        </div>
      </div>
    </div>
  );
}

export default App;
